#!/usr/bin/ruby

##
#  Orbit Socket
#  ----
#  Allows Orbit client communicate with server over websocket.
#  ----
#  Phelps Witter IV (c) 2016
##

require 'socket'

class OrbitSocket
  def initialize(address, port)
    @server = TCPSocket.open(address, port)
    @request = nil
    @response = nil
    send
    listen
    @request.join
    @response.join
  end

  def listen
    @response = Thread.new do
      @server.each_line do |line|
        puts line
      end
    end
  end

  def send
    @request = Thread.new do
      @server.puts(`hostname`)
        msg = ARGV[0]
        @server.puts( msg )
    end
  end
end

orbit = OrbitSocket.new("thesis.local", 4444)
