# Orbit

Orbit Virtual Development Environment (VirtualBox Wrapper)

    usage:  orbit <command> [<virtual machine>] [<subcommand>]

    Commands:

       connect    Connect via ssh to orbit environment
       help       Display this message
       list       Display available orbit environments
       use        Set default orbit environment
       start      Starts orbit environment
       status     Checks status orbit environment
       stop       Safely stops orbit environment

## Why Orbit?
I began developing Orbit to make managing local virtual machines easier. I often found myself tired of typing the same commands over and over again, many of VirtualBox's are rather long, such as starting a VM in headless mode, so to make my life easier I built a simple interface to allow me deal with my VM's with often two word statements. If you want to make dealing with VirtualBox easier (from the command line) Orbit is a great place to start.

## Plans
Immediately I plan on adding the following functionality:

    Commands:

       exec       Execute a command on the virtual machine
       create     Create new virtual machine
       delete     Destroy virtual machine
       -c         interface with linux container (Docker)

### Containers:

I plan to add interfacing with containers, not just on the local machine, but containers located in VM's as well. If on a linux system localhost will be the default container location, if on OS X it will initialize as the default VM. All Orbit commands will work on containers the same way they work on VM's.

### Service:

Orbit will have a service component, located both on the local machine as well as any VM's that are interacted with by Orbit, this is Orbit to work with a port as opposed to using ssh for all it's commands.

